# Singledatabase

Проект предназначен для парсинга русскоязычных и англоязычных патентов (RUPTO/USPTO), журнальных статей и формирования единой базы данных по физической тематике для использования специалистами данной области.

Для работы необходимо установить ПО:

-  Среду разработки Pycharm - https://www.jetbrains.com/pycharm/download/#section=windows
-  Python 3.x - https://www.python.org/downloads
-  MongoDB Community для работы с базой данных -https://www.mongodb.com/try/download/enterprise
-  MongoDB Compass для работы с интерфейсом БД - https://www.mongodb.com/products/compass


Входные данные доступны по ссылке:
https://docs.google.com/document/d/1po-9xgWowNpn6EDFWoaPjIojz1Rk42b0EMcVn4G1EYA/edit?usp=sharing

Для записи данных в БД, необходимо подключиться к серверу:
с помощью командной строки перейти в директорию с MongoDB, перейти по каталогу до папки bin и запустить mongod.

Данные для коннекта с БД:
HOST
localhost:27017
CLUSTER
Standalone
EDITION
MongoDB 4.2.6 Community

Ввиду того, что json-файл с данными патентов для БД превышает лимит 10 Mb, он доступен по ссылке: https://drive.google.com/file/d/1VXUk_OGjEEqVcnepBO20zLpDHH4G1wJj/view?usp=sharing